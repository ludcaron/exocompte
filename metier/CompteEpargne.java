package metier;

/**
 * La classe CompteEpargne représente un compte d'épargne rémunéré.
 *
 * @author Vincent
 * @version 22-oct-2020
 */
public class CompteEpargne extends Compte
{
    private double tauxInteret;


    /**
     * Crée un nouveau compte d'épargne avec un solde nul.
     */
    public CompteEpargne(String numero, Banque banque, Client client, double tauxInteret)
    {
        super(numero, banque, client);
        this.tauxInteret = tauxInteret;
    }
    
    /**
     * Crée un nouveau compte d'épargne avec un solde initial.
     */
    public CompteEpargne(String numero, Banque banque, Client client,
                         double tauxInteret, double solde)
    {
        super(numero, banque, client, solde);
        this.tauxInteret = tauxInteret;
    }
    
    public double getTauxInteret()
    {
        return tauxInteret;
    }
    
    @Override
    public String toString()
    {
        return String.join("\n", super.toString(), "Compte d'épargne", 
            "Taux: " + String.valueOf(tauxInteret));
    }
}
