package metier;

/**
 * La classe DemoExoCompte représente un exemple d'utilisation
 * des classes métier du projet.
 *
 * @author V. Britelle
 * @version 23-oct-2020
 */
public class DemoExoCompte
{

    public void start()
    {
     
        Banque bqSousous = new Banque("Banque Sousous", "Paris");
        Banque bqFlouz = new Banque("Crédit Flouz", "Lyon");
        
        Client prunelle = new Client("Prunelle");
        Client mj = new Client("Mademoiselle Jeanne");
        
        Compte cpt1 = new CompteChèque("00001", bqSousous, prunelle, 5360);
        Compte cpt2 = new CompteEpargne("0002312", bqFlouz, mj, 0.0075, 1200);
        
        System.out.println(cpt1);
        System.out.println(cpt2);
        
        System.out.println("Le compte chèque de Punelle est domicilié à "
            + cpt1.getBanque().getAdresse());
    }
}
