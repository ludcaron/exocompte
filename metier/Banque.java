package metier;
import java.util.Set;
import java.util.HashSet;

/**
 * La classe Banque représente un établissement
 * qui gère des comptes bancaires.
 *
 * @author V. Britelle
 * @version 24-oct-2020
 */
public class Banque
{
    private String nom;
    private String adresse;
    private static Set<Banque> banques = new HashSet<Banque>();

    /**
     * Constructeur d'objets de classe Banque
     */
    public Banque(String nom, String adresse)
    {
        this.nom = nom;
        this.adresse = adresse;
        // Ajout de cette nouvelle banque à la liste de toutes les banques
        // conservée au niveau de la classe (static)
        banques.add(this);
    }

    public String getNom()
    {
        return nom;
    }
    
    public String getAdresse()
    {
        return adresse;
    }
    
    public void setNom(String nom)
    {
        this.nom = nom;
    }
    
    public static Set<Banque> getBanques()
    {
        return banques;
    }
    
    @Override
    public String toString()
    {
        return "La banque " + nom
            + " est domiciliée à " + adresse + ".";
    }
}
