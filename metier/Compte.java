package metier;

/**
 * La classe Compte représente les comptes bancaires individuels.
 *
 * @author V. Britelle
 * @version 22-10-2020
 */
public abstract class Compte
{
    // variables d'instance
    private final Banque banque;
    private final String numero;
    private final Client titulaire;
    private double solde = 0;

    /**
     * Crée un nouveau Compte avec un solde nul.
     */
    public Compte(String numero, Banque banque, Client titulaire)
    {
        this.numero = numero;
        this.banque = banque;
        this.titulaire = titulaire;
    }

    /**
     * Crée un nouveau Compte avec un solde initial
     */
    public Compte(String numero, Banque banque, Client titulaire, double solde)
    {
        this(numero, banque, titulaire);
        this.solde = solde;
    }

    /**
     * Crédite le compte du montant.
     */
    public void credite(double montant)
    {
        solde += montant;
    }

    /**
     * Débite le compte du montant.
     */
    public void debite(double montant)
    {
        solde -= montant;
    }

    public double getSolde()
    {
        return solde;
    }

    public Client getClient()
    {
        return titulaire;
    }

    public Banque getBanque()
    {
        return banque;
    }

    @Override
    public String toString()
    {
        return String.join("\n", "-----\nBanque: " + banque.toString(),
            "Numéro: " + numero,
            "Titulaire: " + titulaire.toString(),
            "Solde: " + String.valueOf(solde)); 
    }
}
