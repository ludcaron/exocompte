package io;
import java.util.Scanner;

/**
 * InputReader reads typed text input from the standard text terminal. 
 * The text typed by a user is then chopped into words, and a set of words 
 * is provided.
 * 
 * @author     Michael Kölling and David J. Barnes
 * @version    0.2 (2016.02.29)
 */
public class InputReader
{
    private Scanner reader;
    private static InputReader inputReader = new InputReader();

    /**
     * Create a new InputReader that reads text from the text terminal.
     */
    private InputReader()
    {
        reader = new Scanner(System.in);
    }

    public static InputReader getInputReader()
    {

        return InputReader.inputReader;
    }

    /**
     * Read a line of text from standard input (the text terminal),
     * and return it as a String.
     *
     * @return  A String typed by the user.
     */
    public String getInput()
    {
        System.out.print("> ");         // print prompt
        String inputLine = reader.nextLine();

        return inputLine;
    }
}
