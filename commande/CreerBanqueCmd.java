package commande;
import io.InputReader;
import metier.Banque;

/**
 * Décrivez votre classe CreerBanqueCmd ici.
 *
 * @author (votre nom)
 * @version (un numéro de version ou une date)
 */
public class CreerBanqueCmd extends Commande
{
    InputReader inputReader = InputReader.getInputReader();
    public void execute()
    {
        String nomBanque = "";
        String adresse = "";
        boolean fini = false;
        do
        {
            System.out.println("Nom de la banque à ajouter : ");
            nomBanque = inputReader.getInput();
            fini = estNomValide(nomBanque);
        }while (!fini);
        
        do
        {
            System.out.println("Adresse de la banque à ajouter : ");
            adresse = inputReader.getInput();
            fini = estNomValide(adresse);
        }while (!fini);
        
        Banque banque = new Banque(nomBanque, adresse);

    }

   private boolean estNomValide(String s)
    {
        return s.length() > 2 && s.length() < 70 ;
    }
}

