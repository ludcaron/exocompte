package commande;


import metier.Banque;
import io.InputReader;
/**
 * Décrivez votre classe ListerBanqueCmd ici.
 *
 * @author (votre nom)
 * @version (un numéro de version ou une date)
 */
public class ListerBanqueCmd extends Commande
{
  InputReader inputReader = InputReader.getInputReader();
    public void execute()
    {
        for (Banque banque : Banque.getBanques())
        {
            System.out.println(banque);
        }
        System.out.println("Appuyez sur Entrée pour revenir au menu...");
        inputReader.getInput();
    }
}
