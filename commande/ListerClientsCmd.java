package commande;
import metier.Client;
import io.InputReader;
public class ListerClientsCmd extends Commande
{
    InputReader inputReader = InputReader.getInputReader();
    public void execute()
    {
        for (Client client : Client.getClients())
        {
            System.out.println(client);
        }
        System.out.println("Appuyez sur Entrée pour revenir au menu...");
        inputReader.getInput();
    }
}