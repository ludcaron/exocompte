package commande;
import io.InputReader;
import metier.Client;
/**
 *
 * @author Vincent
 */
public class CreerClientCmd extends Commande
{
    InputReader inputReader = InputReader.getInputReader();

    public void execute()
    {
        String saisie = "";
        boolean fini = false;
        while (!fini)
        {
            System.out.println("Nom du client à ajouter : ");
            saisie = inputReader.getInput();
            fini = estNomValide(saisie);
        }
        Client client = new Client(saisie);
    }

    private boolean estNomValide(String s)
    {
        return s.length() > 2 && s.length() < 70 ;
    }
}
