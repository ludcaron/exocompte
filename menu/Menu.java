package menu;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 * @author Vincent
 */
public class Menu
{
    // LinkedHashMap keeps the keys in the order they were inserted
    // Utile pour que les menuItem soient affichés dans cet ordre
    Map<String, MenuItem> menuItems = new LinkedHashMap<>();
    
    public void ajouter(String key, MenuItem menuItem)
    {
        menuItems.put(key, menuItem);
    }
    
    public void afficher()
    {
        clearTerminal();
        // manière d'itérer sur un LinkedHashMap: itérer sur les entrées
        for (Map.Entry<String, MenuItem> entry : menuItems.entrySet())
        {
            // entry.getValue() est la valeur de l'entrée, c;-à-d. le MenuItem
            System.out.println(entry.getKey() + ") "
                + entry.getValue().getLabel());
        }
    }
    
    public void realise(String key)
    {
        // Execute la commande associée au menuItem correspondant à la clé fournie
        MenuItem menuItem = menuItems.get(key);
        if (menuItem != null)
            menuItem.getCommande().execute();
    }
    
    private void clearTerminal()
    {
        // Efface le terminal BlueJ
        System.out.print('\u000C');
    }
}
